# Author: dream_solitary
# Date: Feb 16, 2021
# zsh configuration file with ohmyzsh

# set user binary to PATH
if [ -d "$HOME/.local/bin" ] ;then
  export PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/.local/script" ] ;then
  export PATH="$HOME/.local/script:$PATH"
fi
[ -d "$HOME/.cabal/bin" ] && export PATH="$HOME/.cabal/bin:$PATH"

# dmenu scripts (from DT)
[ -d "$HOME/.config/dmscripts/scripts" ] && export PATH="$HOME/.config/dmscripts/scripts:$PATH"

# load environment variables
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/envrc" ] && . "${XDG_CONFIG_HOME:-$HOME/.config}/shell/envrc"

# load proxy settings
[ -f "$XDG_CONFIG_HOME/shell/proxyrc" ] && . "$XDG_CONFIG_HOME/shell/proxyrc"

# oh-my-zsh settings
export ZSH="/home/solitary/.oh-my-zsh"
ZSH_THEME="ys"

plugins=(git zsh-syntax-highlighting git-open)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='nvim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# start Xsession on login
#if [ "$0" = "-zsh" ] && ! [ -n "$SSH_CONNECTION" ] ;then
#  exec startx
#fi

# load alias from file
[ -f "$XDG_CONFIG_HOME/shell/aliarc" ] && . "$XDG_CONFIG_HOME/shell/aliarc"


