-- XMonad config file
-- Author: dream_solitary
-- Date: 02-01-21

-------------------------------------------------------------------------------
-- IMPORT 
import Control.Monad (forM_, join)
import Data.Function (on)
import Data.List (sortBy)
import Data.Monoid
import System.Exit
import System.Directory
import qualified Data.Map as M
import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8

-- XMonad lib
-- Actions
import XMonad
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies, copyToAll)
import XMonad.Actions.CycleWS (toggleWS', moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import qualified XMonad.Actions.TreeSelect
import XMonad.Actions.OnScreen
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.WorkspaceNames
import qualified XMonad.Actions.Search
-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.InsertPosition
-- Layout
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.TrackFloating
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
-- Prompts
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Pass
-- Util
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
-- StackSet
import qualified XMonad.StackSet as W

-- COLOR SCHEME --
miku_magenta = "#e12885"
miku_cyan_light = "#86cecb"
miku_cyan = "#137a7f"
grey = "#373b3e"
light_grey = "#bec8d1"

-------------------------------------------------------------------------------
-- VARIABLES
myFont :: String
myFont = "xft:UbuntuMono Nerd Font Mono:regular:size=12:antialias=true:hinting=true"

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "brave --proxy-server=\"socks5://127.0.0.1:1089\""

dmscripts_path :: String
dmscripts_path = "$HOME/.config/dmscripts/scripts"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myBorderWidth :: Dimension                  -- UI settings
myBorderWidth = 3

myNormalBorderColor  = miku_cyan_light      -- UI color
myFocusedBorderColor = miku_magenta

myModMask :: KeyMask                        -- modkeys
myModMask = mod4Mask

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-------------------------------------------------------------------------------
-- Custom Helpers
-- DynamicLogPP formater for polybar
polybarColor :: String  -- ^ foreground color: a color name, or #rrggbb format
            -> String  -- ^ background color
            -> String  -- ^ output string
            -> String
polybarColor fg bg = wrap t1 t2
  where t1 = concat ["%{F", fg, if null bg then "" else "}%{B" ++ bg, "}"]
        t2 = concat ["%{F-", if null bg then "" else " B-", "}"]

polybarUnderline :: String  -- ^ foreground color: a color name, or #rrggbb format
            -> String  -- ^ output string
            -> String
polybarUnderline fg = wrap t1 t2
  where t1 = concat ["%{u", fg, "}%{+u}"]
        t2 = "%{u-}"

dmscript :: String -> X ()
dmscript cmd = spawn(dmscripts_path ++ "/" ++ cmd)
-------------------------------------------------------------------------------
-- Workspaces Setting (clickable) --
xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
               -- $ [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]
               $ [" task ", " OI ", " doc ", " vid ", " dev ", " vbox ", " sys ", " util "]
  where
        clickable l = [ "%{A1:xdotool key super+" ++ show (n) ++ ":}" ++ ws ++ "%{A}" |
                      (i,ws) <- zip [1..8] l,
                      let n = i ]

-------------------------------------------------------------------------------
-- XMonad Prompts
myXPConfig :: XPConfig
myXPConfig = def
    { font            = myFont
    , position        = Top
    , height          = 28
    , searchPredicate = fuzzyMatch
    }

-------------------------------------------------------------------------------
-- KEY BINDINGS --
myKeys :: String -> [([Char], X ())]
myKeys home = [
         -- Window Actions
           ("M-S-q", killAll)
         , ("M-q", kill1)
         -- , ("M-a", windows copyToAll)
         , ("M-S-a", killAllOtherCopies)
         , ("M-m", windows W.focusMaster)  -- Move focus to the master window
         , ("M-j", windows W.focusDown)    -- Move focus to the next window
         , ("M-k", windows W.focusUp)      -- Move focus to the prev window
         , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
         , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
         , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
         , ("M-<Backspace>", promote)      -- Moves focused window to master, others mintain order
         , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
         , ("M-h", sendMessage Shrink)     -- Shrink horiz window width
         , ("M-l", sendMessage Expand)     -- Expand horiz window width
         , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
         , ("M-M1-k", sendMessage MirrorExpand)          -- Exoand vert window width
         , ("M-`", toggleWS' ["NSP"])
         -- application trigger
         , ("M-M1-<Return>", spawn "$HOME/.config/polybar/current/scripts/launcher.sh")--spawn ("dmenu_run -c -bw 3 -l 12 -sb \"#137a7f\" -nf \"#bec8d1\" -fn \"" ++ myFont ++ "\""))              -- spawn dmenu
         , ("M-<Return>", spawn(myTerminal))  
         , ("M-b", spawn(myBrowser))
         , ("<Print>", spawn("scrot 'screenshot_%Y-%m-%d-%H$%M%S_$wx$h.png' -e 'mv $f ~/image/shots/'"))
         -- XMonad
         , ("M-r", spawn "xmonad --recompile; xmonad --restart")   -- Restarts xmonad
         , ("M-S-<Escape>", spawn("$HOME/.xmonad/exit-all.sh") <+> io exitSuccess)                        -- Quits xmonad
         , ("M-<Escape>", spawn "$HOME/.config/polybar/current/scripts/powermenu.sh")
         -- Increase/decrease windows in the master pane or the stack
         , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase number of clients in master pane
         , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease number of clients in master pane
         , ("M-C-<Up>", increaseLimit)                   -- Increase number of windows
         , ("M-C-<Down>", decreaseLimit)                 -- Decrease number of windows
          -- Layouts
         , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout       
         , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
         --, ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)  -- Toggles noborder
         -- Float
         , ("M-t", withFocused $ windows . W.sink)  -- Push floating window back to tile
         -- TABBED
         , ("M-C-h", sendMessage $ pullGroup L)
         , ("M-C-l", sendMessage $ pullGroup R)
         , ("M-C-k", sendMessage $ pullGroup U)
         , ("M-C-j", sendMessage $ pullGroup D)
         , ("M-C-m", withFocused (sendMessage . MergeAll))
         , ("M-C-u", withFocused (sendMessage . UnMerge))
         , ("M-C-/", withFocused (sendMessage . UnMergeAll))
         , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
         , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab
         -- NamedScratchpad
         , ("M-v", namedScratchpadAction myScratchPads "pulsemixer")
         , ("M-u m", namedScratchpadAction myScratchPads "mocast")
         , ("M-n", namedScratchpadAction myScratchPads "newsboat")
         , ("C-M1-<Return>", namedScratchpadAction myScratchPads "NSPterminal")
         , ("M-c", namedScratchpadAction myScratchPads "mpdctrl")
         -- Prompts
         , ("M-p p", passPrompt myXPConfig)
         , ("M-p g", passGeneratePrompt myXPConfig) -- passGeneratePrompt
         , ("M-p r", passRemovePrompt myXPConfig)   -- passRemovePrompt
         -- dmscripts
         , ("M-d d", dmscript "dm-confedit")
         , ("M-d m", dmscript "dm-music")
         , ("M-d q", dmscript "dm-music -q")
    ]


------------------------------------------------------------------------
-- NamedScratchpad --
myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "pulsemixer" spawnPulsemixer findPulsemixer managePulsemixer
                , NS "mocast" "ppet --no-sandbox" (className =? "PPet") (doSideFloat SE)
                , NS "NSPterminal" spawnTerm findTerm centerFloatM 
                , NS "mpdctrl" "cantata" (className =? "cantata") centerFloatM
                , NS "newsboat" spawnNewsBoat findNewsBoat centerFloatL
                ]
  where
    spawnPulsemixer  = myTerminal ++ " --title pulsemixer --class pulsemixer -e pulsemixer"
    findPulsemixer   = title =? "pulsemixer"
    managePulsemixer = (customFloating $ W.RationalRect 0.45 0.03 0.53 0.17 ) <+> doF W.swapUp
    spawnTerm        = myTerminal ++ " --title NSPTerm"
    findTerm         = title =? "NSPTerm"
    spawnNewsBoat    = myTerminal ++ " --title newsboat --class newsboat -e $HOME/.local/bin/newsboat"
    findNewsBoat     = title =? "newsboat"
    centerFloatM     = (customFloating $ W.RationalRect 0.2 0.2 0.6 0.5) <+> doF W.swapUp
    centerFloatL     = (customFloating $ W.RationalRect 0.2 0.15 0.6 0.7) <+> doF W.swapUp


------------------------------------------------------------------------
-- Layouts:

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i)  True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
master   = renamed [Replace "master"]
           $ trackFloating
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           -- $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 15
           $ ResizableTall 1 (3/100) (2/3) []
binary   = renamed [Replace "tall"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           -- $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 15
           $ ResizableTall 1 (3/100) (1/2) []
--magnify  = renamed [Replace "magnify"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ subLayout [] (smartBorders Simplest)
--           $ magnifier
--           $ limitWindows 12
--           $ mySpacing 8
--           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 1 Full
floats   = renamed [Replace "floats"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           -- $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 0
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
--spirals  = renamed [Replace "spirals"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           -- $ subLayout [] (smartBorders Simplest)
--           $ mySpacing' 6
--           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           -- $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ mySpacing' 4
           $ ThreeCol 1 (3/100) (1/2)
--threeRow = renamed [Replace "threeRow"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           -- $ subLayout [] (smartBorders Simplest)
--           $ limitWindows 7
--           $ mySpacing' 4
--           -- Mirror takes a layout and rotates it by 90 degrees.
--           -- So we are applying Mirror to the ThreeCol layout.
--           $ Mirror
--           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               -- $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
               $ mkToggle (NBFULL ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     binary
                              -- ||| magnify
                                 ||| noBorders monocle
                              -- ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                              -- ||| spirals
                                 ||| threeCol
                                 ||| master
                              -- ||| threeRow
------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = (isDialog --> doF W.swapUp)
               <+> insertPosition Below Newer
               <+> composeAll
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces, and the names would very long if using clickable workspaces.
     [ className =? "Pcmanfm"                      --> doFloat
     , className =? "yakuake"                      --> doFloat
     , className =? "Gimp-2.10"                    --> doFloat
     , title =? "Oracle VM VirtualBox Manager"     --> doFloat
     , className =? "VirtualBox Manager"           --> doShift  ( myWorkspaces !! 5 )
     , title =? "Virtual Machine Manager"          --> doCenterFloat <+> doShift (myWorkspaces !! 5 )
     , className =? "Virt-manager"                 --> doShift (myWorkspaces !! 5 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , className =? "feh"                          --> doFloat
     ]
               <+> manageDocks
               <+> namedScratchpadManageHook myScratchPads
------------------------------------------------------------------------
-- Event handling

-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
-- * NOTE: EwmhDesktops users should use the 'ewmh' function from
-- XMonad.Hooks.EwmhDesktops to modify their defaultConfig as a whole.
-- It will add EWMH event handling to your custom event hooks by
-- combining them with ewmhDesktopsEventHook.
--
--myEventHook = mempty

------------------------------------------------------------------------
-- Log Hooks
polybarPP dbus = def { ppOutput = dbusOutput dbus
                     , ppCurrent = polybarUnderline "#06d6a0" . polybarColor "#06d6a0" "" -- . wrap "[" "]" -- Current workspace in xmobar
                     , ppVisible = polybarColor "#98be65" ""                -- Visible but not current workspace
                     , ppHidden = polybarColor "#82AAFF" "" -- . wrap "*" ""   -- Hidden workspaces in xmobar
                     , ppHiddenNoWindows = polybarColor "#c792ea" ""        -- Hidden workspaces (no windows)
                     , ppTitle = polybarColor "#bec8d1" "" . shorten 60     -- Title of active window in xmobar
                     , ppSep =  "%{F#666} | %{F-}"           -- Separators in xmobar
                     , ppUrgent = polybarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                     , ppExtras  = [windowCount]                           -- # of windows current workspace
                     , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                     }

dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
--
-- * NOTE: EwmhDesktops users should use the 'ewmh' function from
-- XMonad.Hooks.EwmhDesktops to modify their defaultConfig as a whole.
-- It will add initialization of EWMH support to your custom startup
-- hook by combining it with ewmhDesktopsStartup.
--
myStartupHook :: X ()
myStartupHook = do
          spawnOnce "$HOME/.xmonad/startup"  -- startup
          spawnOnce "nitrogen --restore &"   -- wallpaper
          spawnOnce "xcompmgr &"             -- compositor
          spawnOnce "sh -c '/usr/bin/nvidia-settings --load-config-only'"  -- nVIDIA server setting 
          spawnOnce "ibus-daemon -drx"  -- fcitx framework
          spawnOnce "$HOME/.config/polybar/forest/launch.sh" -- start polybar
          spawnOnce "qbittorrent &" -- resume torrents
          --spawnOnce "$HOME/.xmonad/workspace.sh"
--workspacePrepareHook :: X ()
--workspacePrepareHook = do
          
------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main :: IO ()
main = do
    -- Start xmobar
    --xmproc <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobar.hs"
    
    -- DBus session (xmonad-log)
    dbus <- D.connectSession
    -- Request access to the DBus name
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
    -- get home dir
    home <- getHomeDirectory
    -- xmonad
    xmonad $ docks $ ewmh def { 
        handleEventHook = handleEventHook def <+> fullscreenEventHook <+> docksEventHook
        , terminal           = myTerminal
        , focusFollowsMouse  = myFocusFollowsMouse
        , borderWidth        = myBorderWidth
        , modMask            = myModMask
        -- numlockMask deprecated in 0.9.1
        -- numlockMask        = myNumlockMask,
        , workspaces         = myWorkspaces
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        -- hooks, layouts
        , layoutHook         = myLayoutHook
        , manageHook = myManageHook
        , logHook = dynamicLogWithPP (polybarPP dbus)
                   <+> workspaceHistoryHook
                   -- <+> fadeInactiveLogHook 1.0
        , startupHook        = myStartupHook
        } `additionalKeysP` myKeys home

