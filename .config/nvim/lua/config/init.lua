-- manage plugin configurations
require('lfs')
local luaconf = vim.fn.stdpath('config') .. '/lua/config'
for file in lfs.dir(luaconf) do
    if string.find(file, '.lua') ~= nil and file ~= 'init.lua' then -- load all files except init.lua itself
		local file_base_name = string.sub(file, 1, -5)
		if lfs.attributes(luaconf .. '/' .. file_base_name .. '.override') == nil then
	        require('config/' .. file_base_name)
		end
    end
end

